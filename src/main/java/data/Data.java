package data;

import data.clients.Client;
import data.movies.Movie;
import data.rental.Rent;

import java.util.ArrayList;
import java.util.List;

public class Data {
    private List<Movie> moviesList = new ArrayList<>();
    private List<Client> clientsList = new ArrayList<>();
    private List<Rent> rentsList = new ArrayList<>();


    public void addToList(Movie movie){
        moviesList.add(movie);
    }

    public void addToList(Client client){
        clientsList.add(client);
    }

    public void addToList(Rent rents){
        rentsList.add(rents);
    }

    public List<Movie> getMoviesList() {
        return moviesList;
    }

    public void setMoviesList(List<Movie> moviesList) {
        this.moviesList = moviesList;
    }

    public List<Client> getClientsList() {
        return clientsList;
    }

    public void setClientsList(List<Client> clientsList) {this.clientsList = clientsList;}

    public List<Rent> getRentsList() {
        return rentsList;
    }

    public void setRentsList(List<Rent> rentsList) {this.rentsList = rentsList;}
}
