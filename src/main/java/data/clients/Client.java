package data.clients;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;


public class Client implements Comparable<Client>, Serializable {
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private LocalDate birthDate;
    private BigDecimal paymentsOwed;
    private long clientId;

    public Client(String firstName, String lastName, String phoneNumber, String email, LocalDate birthDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.birthDate = birthDate;
    }

    public Client() {
    }

    @Override
    public String toString() {
        return firstName + " " + lastName +
                ", tel. kontaktowy" + phoneNumber +
                ", email :'" + email +
                ", data urodzenia : " + birthDate +
                ", płatności :" + paymentsOwed;
    }

    @Override
    public int compareTo(Client otherClient) {
        return this.lastName.compareTo(otherClient.lastName);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public BigDecimal getPaymentsOwed() {
        return paymentsOwed;
    }

    public void setPaymentsOwed(BigDecimal paymentsOwed) {
        this.paymentsOwed = paymentsOwed;
    }
}
