package data.movies;

import exceptions.NoSuchOptionException;

public enum MovieGenere {
    DRAMA("Dramat"),
    SCIFI("Sci-fi"),
    FANTASY("Fantasy"),
    CRIMINAL("Criminal"),
    THRILLER("Thriler"),
    CHILDREN("Dla dzieci"),
    HISTORY("Historyczny"),
    DOCUMENT("Dokumentalny");

    String description;

    public String getDescription() {
        return description;
    }

    MovieGenere(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }

    public static MovieGenere fromDescription(String description) throws NoSuchOptionException {
        MovieGenere[] values = values();
        for (MovieGenere movieGenere : values) {
            if (movieGenere.getDescription().equals(description))
                return movieGenere;
        }
        throw new NoSuchOptionException("Nie ma takiej opcji : " + description);
    }
}