package data.movies;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Movie Class - to create movie objects
 */

public class Movie implements Comparable<Movie>, Serializable{
    private String movieTitle;
    private int yearOfProduction;
    private ArrayList<MovieGenere> genres;
    private boolean isRented = false;
    private boolean reservation = false;
    private MoviePriceType moviePriceType = MoviePriceType.NEW;
    //private long movieId;

    /**
     * Default Constructor for DataReader
     * @param movieTitle
     * @param yearOfProduction
     * @param genres
     * @param moviePriceType
     */

    public Movie(String movieTitle, int yearOfProduction, ArrayList<MovieGenere> genres, MoviePriceType moviePriceType) {
        this.movieTitle = movieTitle;
        this.yearOfProduction = yearOfProduction;
        this.genres = genres;
        this.moviePriceType = moviePriceType;
    }

    /**
     * to String method using StringBuilder
     * @return String description of object class Movie
     */

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\"" + movieTitle + "\"");
        sb.append(", (" + yearOfProduction + "), ");
        sb.append("Status wypozyczenia : " + isRented);
        sb.append(", Rezerwacja : " + reservation);
        sb.append(", Gatunki : " + genres);
        sb.append(", Kategoria cenowa : " + moviePriceType);
        return  sb.toString();
    }

    @Override
    public int compareTo(Movie otherMovie) {
        return this.movieTitle.compareTo(otherMovie.movieTitle);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return yearOfProduction == movie.yearOfProduction &&
                isRented == movie.isRented &&
                reservation == movie.reservation &&
                movieTitle.equals(movie.movieTitle) &&
                genres.equals(movie.genres) &&
                moviePriceType == movie.moviePriceType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(movieTitle, yearOfProduction, genres, isRented, reservation, moviePriceType);
    }

    //Setters and getters

    public MoviePriceType getMoviePriceType() {
        return moviePriceType;
    }

    public void setMoviePriceType(MoviePriceType moviePriceType) {
        this.moviePriceType = moviePriceType;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public int getYearOfProduction() {
        return yearOfProduction;
    }

    public void setYearOfProduction(int yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }

    public ArrayList<MovieGenere> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<MovieGenere> genres) {
        this.genres = genres;
    }

    public boolean isRented() {
        return isRented;
    }

    public void setRented(boolean rented) {
        isRented = rented;
    }

    public boolean isReservation() {
        return reservation;
    }

    public void setReservation(boolean reservation) {
        this.reservation = reservation;
    }

}