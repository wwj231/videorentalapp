package data.movies;

import exceptions.NoSuchOptionException;

import java.math.BigDecimal;

public enum MoviePriceType {
    REGULAR("Zwykły", new BigDecimal("5.00")),
    NEW("Nowość", new BigDecimal("7.00")),
    BESTSELLER("Hicior", new BigDecimal("9.00"));

    String description;
    BigDecimal price;

    MoviePriceType(String description, BigDecimal price) {
        this.description = description;
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return description + " cena : " + price;
    }

    public static MoviePriceType fromDescription(String description) throws NoSuchOptionException{
        MoviePriceType[] values = values();
        for (MoviePriceType moviePriceType : values) {
            if(moviePriceType.getDescription().equals(description)){
                return moviePriceType;
            }
        }
        throw new NoSuchOptionException("Nie ma takiej opcji : " + description);
    }
}