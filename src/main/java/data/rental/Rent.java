package data.rental;

import data.clients.Client;
import data.movies.Movie;

import java.io.Serializable;
import java.time.LocalDate;

public class Rent implements Comparable, Serializable {
    private  LocalDate rentDate;
    private LocalDate returnDate;
    private Movie movie;
    private Client client;
    boolean overDate = false;
    private int rentId;


    public Rent(LocalDate rentDate, LocalDate returnDate, Movie movie, Client client) {
        this.rentDate = rentDate;
        this.returnDate = returnDate;
        this.movie = movie;
        this.client = client;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\"" + movie.getMovieTitle() + "\"");
        sb.append(", Data wypozyczenia : " + rentDate);
        sb.append(", Przewidywana data zwrotu : " + returnDate);
        sb.append(", Klient : " + client.getLastName() + " " + client.getFirstName());
        sb.append(", Po terminie? : " + overDate);
        return sb.toString();
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }

    public LocalDate getRentDate() {
        return rentDate;
    }

    public void setRentDate(LocalDate rentDate) {
        this.rentDate = rentDate;
    }

    public LocalDate getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDate returnDate) {
        this.returnDate = returnDate;
    }


    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
