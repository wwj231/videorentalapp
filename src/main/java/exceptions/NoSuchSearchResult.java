package exceptions;

public class NoSuchSearchResult extends Exception {
    public NoSuchSearchResult(String message) {
        super(message);
    }
}
