package io;

import app.Option;
import data.clients.Client;
import data.movies.Movie;
import data.movies.MovieGenere;
import data.movies.MoviePriceType;
import exceptions.NoSuchOptionException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class DataReader {
    private Scanner sc = new Scanner(System.in);
    private ConsolePrinter consolePrinter = new ConsolePrinter();

    /**
     * Closes Scanner stream.
     */
    public void close() {
        sc.close();
    }


    /**
     * To read integer values with "consuming" next line sign.
     * @return integer
     */
    public int getInt() {
        int number = sc.nextInt();
        sc.nextLine();
        return number;
    }

    /**
     * To read from console using Scanner
     * @return String
     */
    public String getString(){
        return sc.nextLine();
    }

    public Option getOptionMenu(){
        boolean optionOk = false;
        Option option = null;
        while (!optionOk){
            try {
                option = Option.fromDescription(sc.nextLine().toUpperCase());
                optionOk = true;
            } catch (NoSuchOptionException e){
                System.out.println(e.getMessage() + " , podaj jeszcze raz");
            }
        }
        return option;
    }

    public Client readAndCreateClient(){
        System.out.println("Imię :");
        String firstName = sc.nextLine();
        System.out.println("Nazwisko :");
        String lastName = sc.nextLine();
        System.out.println("Numer telefonu do kontaktu : ");
        String phoneNumber = sc.nextLine();
        System.out.println("Adres email :");
        String email = sc.nextLine();
        System.out.println("Data urodzenia : ");
        String birthDate = sc.nextLine();
        return new Client(firstName, lastName, phoneNumber, email, LocalDate.parse(birthDate));
    }

    public Movie readAndCreateMovie() {
        System.out.println("Tytuł: ");
        String movieTitle = sc.nextLine();

        System.out.println("Rok produkcji: ");
        int yearOfProduction = getInt();
        String input;

        ArrayList<MovieGenere> movieGeneres = new ArrayList<>();
        do {
            System.out.println("Gatunek filmu : ");
            consolePrinter.displayEnumValues(MovieGenere.class);
            System.out.println();
            movieGeneres.add(getMovieGenre());
            System.out.println("K - zakoncz wporwadzanie, dowlny klawisz by kontunuować");
            input = sc.nextLine();
        } while(!input.equals("K"));

        System.out.println("Wporwadz kategorie cenowa :");
        consolePrinter.displayEnumValues(MoviePriceType.class);

        MoviePriceType moviePriceType = getMoviePriceType();

        return new Movie(movieTitle, yearOfProduction, movieGeneres, moviePriceType);
    }

    /*
        Działa, ale nie podoba mi się to, mogłoby być lepiej. Obie metody praktycnie robią to samo.
        Można by to spróbować rozwiązać jakąś metodą generyczną. Problem, że trzeba by wywalić metodę
        fromDescription, albo przepisać tak, żeby można było tworzyć nowe enum w oparciu o standardowe metody
        Klasy Enum
    */

    private MoviePriceType getMoviePriceType(){
        MoviePriceType moviePriceType = null;
        boolean optionOk = false;
        while (!optionOk) {
            try {
                moviePriceType = MoviePriceType.fromDescription(sc.nextLine());
                optionOk = true;
            } catch (NoSuchOptionException e) {
                System.out.println(e.getMessage() + " , podaj jeszcze raz");
            }
        }
        return moviePriceType;
    }

    private MovieGenere getMovieGenre(){
        MovieGenere movieGenere = null;
        boolean optionOk = false;
        while (!optionOk) {
            try {
                movieGenere = MovieGenere.fromDescription(sc.nextLine());
                optionOk = true;
            } catch (NoSuchOptionException e) {
                System.out.println(e.getMessage() + " , podaj jeszcze raz");
            }
        }
        return movieGenere;
    }

}