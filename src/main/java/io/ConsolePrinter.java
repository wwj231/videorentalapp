package io;

import app.Option;
import de.vandermeer.asciitable.AsciiTable;
import data.Data;
import java.util.EnumSet;
import java.util.stream.Collectors;

public class ConsolePrinter {
    AsciiTable at = new AsciiTable();

    public String mainMenu(){
        at.addRule();
        at.addRow("OBSŁUGA WYPOZYCZEN", "BAZA FILMOW", "BAZA KLIENTOW");
        at.addRule();
        at.addRow(Option.RENT_FILM, Option.DISPLAY_FILMS, Option.DISPLAY_CLIENTS);
        at.addRule();
        at.addRow(Option.RETURN_FILM, Option.ADD_FILM, Option.ADD_CLIENT);
        at.addRule();
        at.addRow(Option.BOOK_FILM, Option.SEARCH_FILM, Option.SEARCH_CLIENT);
        at.addRule();
        at.addRow(Option.OVERDATE_RENT, " ", Option.MODIFY_CLIENT);
        at.addRule();
        at.addRow(" ", " ", Option.EXIT);
        at.addRule();
        String rend = at.render();
        return rend;
    }

    public void displayMovies(Data data){ data.getMoviesList().forEach(e -> System.out.println(e)); }

    public void displayClients(Data data) { data.getClientsList().forEach(e -> System.out.println(e)); }

    public <T extends Enum<T>> void displayEnumValues(Class<T> clazz) {
        System.out.println(EnumSet.allOf(clazz).stream()
                .map(Enum::toString)
                .collect(Collectors.joining(", " , "\"[", "]\"")));
    }
}