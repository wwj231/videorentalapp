package io.file;

import data.clients.Client;
import data.movies.Movie;
import data.rental.Rent;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public interface FileManager {

    /*List importMovieData() throws IOException;
    void exportMovieData(List<Movie> list);*/

    /*ArrayList<Client> importClientData() throws IOException;
    void exportClientData(ArrayList<Client> list);

    ArrayList<Rent> importRentData() throws IOException;
    void exportRentData(ArrayList<Rent> list);*/

    <T extends Object> List<T> importData(Class<T> tClass, String filePath) throws IOException;
    <T extends Object> void exportData(List<T> list, String filePath);
}
