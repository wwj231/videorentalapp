package io.file;

import java.io.*;
import java.util.List;

public class SerializableFileManager implements FileManager {

    /**
     * Generic method to import data to list from file with serialized data
     * @param tClass Model class of objects to be imported from file.
     * @param filePath Path to file that contains serialized data
     * @param <T> Name of the model class.
     * @return Returns list of objects, each casted to @param tClass provieded
     * @throws IOException, handles FileNotFoundException and ClassNotFoundException
     * (if data stored in file is naot oftClass type.
     */
    @Override
    public <T> List<T> importData(Class<T> tClass, String filePath) throws IOException {
        try(var ois = new ObjectInputStream(new FileInputStream(filePath))) {
            return (List<T>) ois.readObject();
        } catch (FileNotFoundException e){
            System.out.println("Nie znaleziono pliku: " + filePath);
        } catch (ClassNotFoundException e){
            System.out.println("Niezgodny typ danych: " + filePath);
            e.printStackTrace();
        }
        throw new IOException();
    }

    /**
     * Generic method to export data objects from list to file with serialized data
     * @param list
     * @param filePath
     * @param <T>
     */
    @Override
    public <T> void exportData(List<T> list, String filePath){
        try(var oos = new ObjectOutputStream(new FileOutputStream(filePath))){
            oos.writeObject(list);
        } catch (FileNotFoundException e){
            System.out.println("Nie znaleziono pliku " + filePath);
        } catch (IOException e){
            System.out.println("Błąd zapisu danych do pliku " + filePath);
        }
    }
}
