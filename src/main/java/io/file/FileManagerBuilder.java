package io.file;

import exceptions.NoSuchOptionException;
import io.DataReader;

public class FileManagerBuilder {
    DataReader dataReader = new DataReader();

    public FileManagerBuilder(DataReader dataReader) {
        this.dataReader = dataReader;
    }

    public FileManager defaultBuild(){
        return new SerializableFileManager();
    }

    public FileManager exportBuild() throws NoSuchOptionException{
        System.out.println("Wybierz format danych:");
        FileType fileType = getFileType();
        switch (fileType) {
            case JSON:
                return new JsonFileManager();
            default:
                throw new NoSuchOptionException("Nieobsługiwany typ danych");
        }
    }

    private FileType getFileType() {
        boolean typeOk = false;
        FileType result = null;
        do {
            printTypes();
            String type = dataReader.getString().toUpperCase();
            try {
                result = FileType.valueOf(type);
                typeOk = true;
            } catch (IllegalArgumentException e) {
                System.out.println("Nieobsługiwany typ danych, wybierz ponownie.");
            }
        } while (!typeOk);

        return result;
    }

    private void printTypes() {
        for (FileType value : FileType.values()) {
            System.out.println(value.name());
        }
    }
}
