package io.file;

public enum FileType {
    JSON, CSV, SERIAL;
}
