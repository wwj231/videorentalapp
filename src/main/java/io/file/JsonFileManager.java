package io.file;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class JsonFileManager implements FileManager {

    /**
     * @param tClass
     * @param filePath
     * @param <T>
     * @return
     * @throws IOException
     */
    @Override
    public <T> List<T> importData(Class<T> tClass, String filePath) throws IOException {
        String jsonArrString = null;
        StringBuilder sb = new StringBuilder();

        try (var reader = new BufferedReader(new FileReader(filePath))) {
            String nextLine;
            while ((nextLine = reader.readLine()) != null) {
                sb.append(nextLine);
            }
            jsonArrString = sb.toString();
        } catch (FileNotFoundException e){
            System.out.println("Nie znaleziono pliku: " + filePath);
        }

            ObjectMapper objectMapper = new ObjectMapper();//new TypeReference<List<T>>(){}
            return objectMapper.readValue(jsonArrString, objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, tClass));
    }

    /**
     * @param list
     * @param filePath
     * @param <T>
     */
    @Override
    public <T> void exportData(List<T> list, String filePath) {


        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValue(new File(filePath), list);
        } catch (IOException e) {
            System.out.println("Zapis sie nie powiódł");
            e.printStackTrace();
        }
    }

    /*@Override
    public ArrayList<Client> importClientData() throws IOException {
        String jsonArrString = null;
        String nextLine = null;
        StringBuilder sb = new StringBuilder();

        try (var reader = new BufferedReader(new FileReader(clientsFilePath))){
            while ((nextLine = reader.readLine())!=null) {
                sb.append(nextLine);
            }
            jsonArrString = sb.toString();
        }

        ObjectMapper objectMapper = new ObjectMapper();
        ArrayList<Client> list = objectMapper.readValue(jsonArrString,new TypeReference<ArrayList<Client>>(){});
        return list;
    }

    @Override
    public void exportClientData(ArrayList<Client> list) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValue(new File(clientsFilePath), list);
        } catch (IOException e) {
            System.out.println("Zapis sie nie powiódł");
            e.printStackTrace();
        }
    }*/
}