package app;

public class VideoRentalApp {
    final static String versionTitle= "VideoRentalApp v0.1";

    public static void main(String[] args) {
        AppControl appControl = new AppControl();
        System.out.println(versionTitle);
        appControl.controlLoop();
    }
}
