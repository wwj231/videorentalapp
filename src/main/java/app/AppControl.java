package app;

import data.Data;
import data.clients.Client;
import data.movies.Movie;
import data.rental.Rent;
import io.ConsolePrinter;
import io.DataReader;
import io.file.FileManager;
import io.file.FileManagerBuilder;

import java.io.FileNotFoundException;
import java.io.IOException;

class AppControl {
    private static final String moviesFilePathSerializable = "./data_files/movies.o";
    private static final String clientsFilePathSerializable = "./data_files/clients.o";
    private static final String rentFilePathSerializable = "./data_files/rentals.o";

    private DataReader dataReader = new DataReader();
    private ConsolePrinter consolePrinter = new ConsolePrinter();
    private Data data = new Data();
    private FileManager fileManager;

    AppControl() {
        fileManager = new FileManagerBuilder(dataReader).defaultBuild();
        loadMovieData();
        loadClientsData();
        loadRentData();
    }

    void controlLoop() {
        Option option;
        String mainMenu = consolePrinter.mainMenu();

        do {
            System.out.println(mainMenu);
            option = dataReader.getOptionMenu();
            switch (option) {
                case RENT_FILM:

                    break;
                case RETURN_FILM:

                    break;
                case BOOK_FILM:

                    break;
                case OVERDATE_RENT:

                    break;
                case DISPLAY_FILMS:
                    consolePrinter.displayMovies(data);
                    dataReader.getString();
                    break;
                case ADD_FILM:
                    data.addToList(dataReader.readAndCreateMovie());
                    fileManager.exportData(data.getMoviesList(), moviesFilePathSerializable);
                    break;
                case SEARCH_FILM:
                    break;
                case ADD_CLIENT:
                    data.addToList(dataReader.readAndCreateClient());
                    break;
                case DISPLAY_CLIENTS:
                    consolePrinter.displayClients(data);
                    break;
                case SEARCH_CLIENT:
                    break;
                case EXIT:
                    exit();
                    break;
                default:
                    System.out.println("Nie ma takiej opcji, wprowadź ponownie: ");
            }
        } while (option != Option.EXIT);
    }

    private void exit() {
        System.out.println("Koniec programu, papa!");
        dataReader.close();
    }

    private void loadMovieData(){
        try {
            data.setMoviesList(fileManager.importData(Movie.class, moviesFilePathSerializable));
            System.out.println("Zaimportowano dane z pliku");
        } catch (FileNotFoundException e){
            System.out.println("Nie znaleziono plików z bazami. Dane zostaną zapisane do noowo utworzonych plików");
        } catch (IOException e) {
            System.out.println("Nie udało się wczytać pliku");
            e.getStackTrace();
        }

    }

    private void loadClientsData(){
        try {
            data.setClientsList(fileManager.importData(Client.class, clientsFilePathSerializable));
            System.out.println("Zaimportowano dane z pliku");
        } catch (IOException e) {
            System.out.println("Nie udało się wczytać pliku");
            e.getStackTrace();
        }
    }

    private void loadRentData(){
        try {
            data.setRentsList(fileManager.importData(Rent.class, rentFilePathSerializable));
            System.out.println("Zaimportowano dane z pliku");
        } catch (IOException e) {
            System.out.println("Nie udało się wczytać pliku z ifnromacjami o wypozyczeniach");
            e.getStackTrace();
        }
    }
}
