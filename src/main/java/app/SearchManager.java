package app;

import data.clients.Client;
import data.movies.Movie;
import data.rental.Rent;
import exceptions.NoSuchSearchResult;

import java.util.List;

public class SearchManager implements Searchable{

    @Override
    public Movie searchInMovies(List<Movie> movies, String title) throws NoSuchSearchResult{
        for (Movie m : movies) {
            if(title.equals(m.getMovieTitle())){
                return m;
            }
        }
        throw new NoSuchSearchResult("Nie znaleziono szukanego tytułu");
    }

    @Override
    public Client searchInClients(List<Client>clients, String firstName, String lastName) throws NoSuchSearchResult {
        for (Client c : clients) {
            if(lastName.equals(c.getLastName()) && firstName.equals(c.getLastName())){
                return c;
            }
        }
        throw new NoSuchSearchResult("Nie znaleziono szukanego klienta");
    }

    @Override
    public Rent searchInRents(Movie movie) {
        return null;
    }
}
