package app;

import data.clients.Client;
import data.movies.Movie;
import data.rental.Rent;
import exceptions.NoSuchSearchResult;

import java.util.List;

public interface Searchable {
    public Movie searchInMovies(List<Movie> list, String title) throws NoSuchSearchResult;
    public Client searchInClients(List<Client> clients, String firstName, String lastName) throws NoSuchSearchResult;
    public Rent searchInRents(Movie movie) throws NoSuchSearchResult;
}
