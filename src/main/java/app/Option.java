package app;

import exceptions.NoSuchOptionException;

import java.util.NoSuchElementException;

public enum Option {
    RENT_FILM ("W", "WYPOŻYCZENIE FILMU"),
    RETURN_FILM("Z", "ZWROT FILMU"),
    BOOK_FILM("R", "REZERWACJA FILMU"),
    OVERDATE_RENT("P", "PRZETERMINOWANE ZWROTY"),
    DISPLAY_FILMS("M", "WYSWIETL BAZE FILMOW"),
    ADD_FILM("DM", "DODAJ POZYCJE"),
    SEARCH_FILM("WM", "WYSZUKAJ FILM"),
    DISPLAY_CLIENTS("K", "WYSWIETL BAZE KLIENTOW"),
    ADD_CLIENT("DK", "DODAJ KLIENTA"),
    SEARCH_CLIENT("WK", "WYSZUKAJ KLIENTA"),
    MODIFY_CLIENT("MK", "ZMIEŃ DANE KLIENTA"),
    EXIT("EXIT", "WYJŚCIE");

    private String description;
    private String descriptionMenu;

    public String getChoice() {
        return description;
    }

    public String getDescriptionMenu() {
        return descriptionMenu;
    }

    Option(String choice, String descriptionMenu) {
        this.description = choice;
        this.descriptionMenu = descriptionMenu;
    }

    @Override
    public String toString() {
        return description + " - " + descriptionMenu;
    }

    public static Option fromDescription(String menuChoice) throws NoSuchOptionException {
        Option[] values = values();
            for (Option option : values) {
                if (option.description.equals(menuChoice))
                    return option;
            }
            throw new NoSuchOptionException("Nie ma takiej opcji : " + menuChoice);
    }
}
