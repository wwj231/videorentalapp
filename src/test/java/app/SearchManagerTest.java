package app;

import data.movies.Movie;
import data.movies.MovieGenere;
import data.movies.MoviePriceType;
import exceptions.NoSuchSearchResult;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class SearchManagerTest {


    @Test
    public void searchInMoviesShouldFindFilm() throws NoSuchSearchResult {
        //given
        MovieGenere movieGenere = MovieGenere.DRAMA;
        ArrayList<MovieGenere> movieGeneres = new ArrayList<>();
        movieGeneres.add(movieGenere);
        Movie movie1 = new Movie("Film1", 1986, movieGeneres, MoviePriceType.NEW);
        Movie movie2 = new Movie("Film2", 1986, movieGeneres, MoviePriceType.BESTSELLER);
        Movie movie3 = new Movie("Film3", 1986, movieGeneres, MoviePriceType.REGULAR);
        List<Movie> movies = new ArrayList<>();
        movies.add(movie1);
        movies.add(movie2);
        movies.add(movie3);
        SearchManager searchManager = new SearchManager();
        String searchedTitle = "Film1";
        //when
        assertEquals(movie1, searchManager.searchInMovies(movies, searchedTitle));
    }

    @Test(expected = NoSuchSearchResult.class)
    public void searchInMoviesShouldThrow() throws NoSuchSearchResult {
        //given
        MovieGenere movieGenere = MovieGenere.DRAMA;
        ArrayList<MovieGenere> movieGeneres = new ArrayList<>();
        movieGeneres.add(movieGenere);
        Movie movie1 = new Movie("Film1", 1986, movieGeneres, MoviePriceType.NEW);
        Movie movie2 = new Movie("Film2", 1986, movieGeneres, MoviePriceType.BESTSELLER);
        Movie movie3 = new Movie("Film3", 1986, movieGeneres, MoviePriceType.REGULAR);
        List<Movie> movies = new ArrayList<>();
        movies.add(movie1);
        movies.add(movie2);
        movies.add(movie3);
        SearchManager searchManager = new SearchManager();
        String searchedTitle = "Film40";
        //when
        searchManager.searchInMovies(movies, searchedTitle);
    }

    @Test
    public void searchInClients() {
    }

    @Test
    public void searchInRents() {
    }
}